#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <queue>
#include <stack>
#include <set>
#include <iomanip> 

using namespace std;

// this is the number of numbers you want to check
int num = 1000005;
// this is the array where 0 means prime, and 1 means not prime for given indexx i (prime[i])
int prime[1000005];
 
int main(){
    // if you want to factorize a number, and store its factors, then b should be that number
    long long int b;
    cin>>b;

    for(int i=0;i<num;i++){
        prime[i] = 0;
    }
    // operate the sieve, prime[i]==0 means i is prime, and prime[i]==1 means i is not prime
    // its complexity is num*log(log(num)) (to be safe assume num*log(num) )
    for(long long int i=2;i<num;i++){
        if(prime[i]==0){
            
            for(long long int j=i*i;j<num;j+=i){
                prime[j] =1;
            }
        }
    }

    // we store thre factors of b in the following array. make sure that b can be at maximum (num-1)^2
    // we store it as pairs <factors, number of times that factor occurs in b>
    vector < pair < long long int, int > > factors;
    long long int x = b;
    for(int i=2;i<num;i++){
        if(prime[i]==0){
            if(x%i==0){
                int total = 0;
                while(x%i==0){
                    x = x/i;
                    total++;
                }
                // cout<<i<<" "<<total<<"\n";       
                factors.push_back(make_pair(i,total));
            }
        }
    }
    if(x>1){
        factors.push_back(make_pair(x,1));
    }

    /*
    ****************************************************************************************************************************
    start your own code here
    ****************************************************************************************************************************
    */


}